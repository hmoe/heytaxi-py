# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# Project: Hey Taxi!
# License: BSD-3, see LICENSE.md
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
from importlib import resources
from typing import Final, Self
import pygame
from pygame.surface import Surface

TILESET_COLUMNS = 16
TILESET_ROWS = 16


class Tileset:
    def __init__(self, surface: Surface) -> None:
        """Initialize the `Tileset` with the given `surface`. This tileset
        assumes that the surface defines 16 times 16 (256) tiles aranged
        in a square. Therefor the surface-size must be a multiple of 16 in
        both dimensions."""
        self.surface: Final = surface
        self.tile_width: Final = surface.get_width() // TILESET_COLUMNS
        self.tile_height: Final = surface.get_height() // TILESET_ROWS

    @classmethod
    def from_resources(cls, name: str) -> Self:
        """Loads the surface from the package-resources."""
        container = resources.files("heytaxi.view.resources.graphics")
        with resources.as_file(container.joinpath(name)) as fp:
            surface = pygame.image.load(fp, name)
            return cls(surface)

    def get(self, column: int, row: int) -> Surface:
        """Retrieve a subsurface from the tileset identified by its
        `column` and `row`."""
        return self.surface.subsurface(
            pygame.rect.Rect(
                column * self.tile_width,
                row * self.tile_height,
                self.tile_width,
                self.tile_height,
            )
        )
