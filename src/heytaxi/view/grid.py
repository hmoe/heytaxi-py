# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# Project: Hey Taxi!
# License: BSD-3, see LICENSE.md
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
import random
from collections.abc import MutableSequence
import pygame
from .tiles import Tileset


class Grid:
    """Manage a grid of sprites to be drawn onto the screen. This view will
    be used to display most of the game's static screen contents."""

    def __init__(self, columns: int, rows: int, tileset: Tileset) -> None:
        """Initialize the `Grid` object to manage `columns` times `rows`
        cells using the given `tileset` to fetch the images from."""
        self.columns = columns
        self.rows = rows
        self.tileset = tileset
        self.group: pygame.sprite.Group = pygame.sprite.Group()
        self.cells: MutableSequence[pygame.sprite.Sprite] = []
        for y in range(rows):
            for x in range(columns):
                ## Experimental (use random tiles)
                character = random.randint(ord(" "), ord("~"))
                tileset_row = character // 16
                tileset_col = character % 16
                ## ---
                sprite = pygame.sprite.Sprite()
                sprite.image = self.tileset.get(tileset_col, tileset_row)
                sprite.rect = pygame.rect.Rect(
                    x * 16,
                    y * 16,
                    16,
                    16,
                )
                self.cells.append(sprite)
                self.group.add(sprite)

    def draw(self, surface: pygame.surface.Surface) -> None:
        self.group.draw(surface)
