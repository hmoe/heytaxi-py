# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# Project: Hey Taxi!
# License: BSD-3, see LICENSE.md
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
from dataclasses import dataclass


@dataclass(frozen=True)
class Configuration:
    """Definition of the application's configuration and settings."""

    title = "Hey, Taxi!"
    size = 1024, 768  # multiples of 16
    fps = 60


def get() -> Configuration:
    """Provide the application's configuration."""
    return Configuration()
