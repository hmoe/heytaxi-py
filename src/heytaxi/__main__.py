# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# Project: Hey Taxi!
# License: BSD-3, see LICENSE.md
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# Project: Hey Taxi!
# License: BSD-3, see LICENSE.md
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
import pygame
from . import config
from .view import tiles  # EXPERIMENTAL
from .view import grid  # EXPERIMENTAL


class Application:
    """Provide the context (SDL-window) and the event processing loop for
    the application to run within."""

    def __init__(self, configuration: config.Configuration) -> None:
        """Initialize the `Application`-object."""
        self.config = configuration
        self.running = True
        # Following "declarations" will be initialized inside the `on_start`
        # method. These are declared in the `__init__`-method for type
        # checking and better readablity.
        self.screen: pygame.surface.Surface
        self.clock: pygame.time.Clock

    def mainloop(self) -> None:
        """Execute the event processing loop and call appropitate methods
        according to the application's life cycle."""
        delta_time = 0.0
        self.on_start()

        while self.running:
            delta_time = self.clock.tick(self.config.fps) / 1000
            for event in pygame.event.get():
                self.on_event(event)
            self.on_update(delta_time)
            self.on_render()
            pygame.display.flip()

        self.on_exit()

    # - - - Life cycle hooks - - -

    def on_render(self) -> None:
        """Called by `mainloop` right after the `on_update` was called. Here
        renderable objects may be drawn."""
        self.screen.fill("black")
        # EXPERIMENTAL:
        self.grid.draw(self.screen)

    def on_event(self, event: pygame.event.Event) -> None:
        """Called by `mainloop` for every event fetched from the event
        queue."""
        if event.type == pygame.QUIT:
            self.running = False

    def on_exit(self) -> None:
        """Called once by `mainloop` after leaving the event processing
        loop."""

    def on_start(self) -> None:
        """Called once by `mainloop` before entering the event processing
        loop."""
        self.screen = pygame.display.set_mode(self.config.size)
        self.clock = pygame.time.Clock()
        pygame.display.set_caption(self.config.title)
        # EXPERIMENTAL:
        self.tileset = tiles.Tileset.from_resources("ascii16x16.png")
        self.grid = grid.Grid(1024 // 16, 768 // 16, self.tileset)

    def on_update(self, delta_time: float) -> None:
        """Called by `mainloop` for every tick. `delta_time` is the time
        elapsed since the last call in seconds."""


def main() -> None:
    """This entry-point create and execute the application's context."""
    Application(config.get()).mainloop()


if __name__ == "__main__":
    main()
