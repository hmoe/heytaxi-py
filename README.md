# Hey Taxi!

In this arcarde game you'll convey passengers around in your flying taxi.

## Work in progress

This is a work in progress. Right now it's nothing more than a
black-window-simulator (now with random images).

### Installation steps:

1. Clone this repository into a local directory.
2. Change into the project directory and create a virtual environment (venv).
3. Activate the venv.
4. Execute the program.

Installation on windows:
```
PS > git clone https://gitlab.com/hmoe/heytaxi-py
PS > cd .\heytaxi-py
PS > python -m venv venv
PS > .\venv\Scripts\activate
(venv) PS > pip install -e .
(venv) PS > python -m heytaxi
```

The `-e` switch for the pip command creates an editable install. This is
usefull for development.

To install the development environment run `pip install -e .[develop]` to
install the optional dependencies. Right now I am using `mypy`, `pytest` and
`ruff`.

### Current status

I have implemented a simple resource loader for tileset which access the
package resources inside `heytaxi.view.resources` along side with a simple
`Grid` class which manages sprites to draw tiles.

At the moment random tiles will be drawn onto the main screen surface to
demonstrate/test these functionalities. 

Unfortunately `pygame` does not support per sprite color blendung (AFAIK).
As a result the `Grid` class must be changed to create independend colored
surfaces per sprite to archive some basic coloring.
